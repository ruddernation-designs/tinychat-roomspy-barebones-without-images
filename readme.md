# TinyChat-RoomSpy-Barebones-Without-Images
This is TinyChat's Room Spy Barebones created by Ruddernation Designs 2015,
This contains no CSS so you'll have to modify as you wish, I've added very basic HTML,
This versions just shows users in the room, It does not get the pictures if they're on video/audio, 
It will show room image though,
This also lets you know user count, moderator count and how many are broadcasting, also if the room is passworded.
<br />
###<a href="https://www.tinychat-spy.com" target="_blank">Demo</a>
